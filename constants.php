<?php
define('PARAM_LIST_DIR', 'dir');
define('PARAM_SUB_FILE', 'file');
define('PARAM_PLAYER_FILE', 'file');

define('BASE_DIR', __DIR__);
define('BASE_URL', 'http://'.$_SERVER['HTTP_HOST'].'/mytube');
define('PLAYER_URL', BASE_URL.'/player.php?'.PARAM_PLAYER_FILE.'=');
define('LIST_URL', BASE_URL.'/list.php?'.PARAM_LIST_DIR.'=');
define('SUBTITLE_URL', BASE_URL.'/subtitle.php?'.PARAM_SUB_FILE.'=');
define('VIDEO_URL', BASE_URL.'/video.php?'.PARAM_PLAYER_FILE.'=');
define('IMAGE_URL', BASE_URL.'/image.php/'.PARAM_PLAYER_FILE.'=');

$VIDEOS_DIRS = array('/home/neuber/Vídeos', '/home/neuber/Torrent/Completo'); 
$PREVIEW_NAMES = array('cover', 'preview');
$FORBIDEN_EXTENSIONS = array('jpg', 'png', 'jpeg', 'gif', 'srt', 'iso', 'dat', 'nfo', 'md5', 'txt', 'log');
