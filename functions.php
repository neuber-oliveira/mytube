<?php
include 'constants.php';

function listDir($dirs){
	global $PREVIEW_NAMES, $FORBIDEN_EXTENSIONS;
	$filesList = array('dirs'=>array(), 'files'=>array(), 'preview'=>null);
	
	foreach($dirs as $dir){
		$list = scandir($dir);
		
		foreach( $list as $file ){
			if( preg_match('/^\./', $file)==0 ){
				$path = $dir.'/'.$file;
				if( is_dir($path) ){
					$filesList['dirs'][] = $path;
				}else if( is_file($path) &&  preg_match('/\.'.implode('|', $FORBIDEN_EXTENSIONS).'$/', $file)==0 ){
					$filesList['files'][] = $path;
				}
			}
		}
	}
	
	return $filesList;
}

function getCover($dir, $default=null){
	global $PREVIEW_NAMES;
	$exts = array('jpg', 'png');
	
	$file = null;
	foreach( $PREVIEW_NAMES as $preview ){
		foreach( $exts as $ext ){
			$path = $dir.'/'.$preview.'.'.$ext;
			if( file_exists($path) ){
				$file = $path;
				break 2;
			}
		}
	}
	
	if( empty($file) && $default){
		$file = BASE_URL.'/images/'.$default;
	}
	
	return $file;
}

function getImageUrl($path, $width=350, $height=100, $zc=1){
	return BASE_URL."/timthumb.php?w=$width&h=$height&zc=$zc&src=$path";
}

function getSubtitleUrl($videoFile){
	$dir = dirname($videoFile);
	$video = basename($videoFile);
	$videoNoExt = substr($video, 0, strrpos($video, '.') );
	$subs = array('srt', 'vtt');
	$subUrl = null;
	foreach( $subs as $sub ){
		$subname = $videoNoExt.'.'.$sub;
		$subPath = $dir.'/'.$subname;
		if( file_exists($subPath) ){
			$subUrl = SUBTITLE_URL.$subPath;
			break;
		}
	}
	
	return $subUrl;
}

function formatName($name){
	return str_replace(array('-', '.', ','), array(' ', ' ', ', '), $name);
}
