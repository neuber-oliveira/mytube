<?php
include 'constants.php';

$targetFile = null;
if( isset($_GET[PARAM_SUB_FILE]) ){
	if( file_exists($_GET[PARAM_SUB_FILE]) ){
		$targetFile = $_GET[PARAM_SUB_FILE];
	}
}
$lines = file($targetFile);

foreach( $lines as $i=>$line){
	if( preg_match('/^\s?(\d{1,2}):(\d{1,2})/', $line) ){
		$lines[$i] = str_replace(',', '.', $line);
	}
}

if( preg_match('/^WEBVTT/', $lines[0])==0 )
	array_unshift($lines, "WEBVTT\n");

//var_dump($filename);
header('Content-type: text/plain');
header('Content-Length: '.filesize($targetFile));
echo utf8_encode( implode("", $lines) );
