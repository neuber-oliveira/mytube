<?php
include 'functions.php';

$targetDirs = $VIDEOS_DIRS;

if( isset($_GET[PARAM_LIST_DIR]) ){
	if( file_exists($_GET[PARAM_LIST_DIR]) ){
		$targetDirs = array( $_GET[PARAM_LIST_DIR] );
	}
}

$filesList = listDir($targetDirs);
?>

<?php include 'includes/header.php' ?>
		<div data-equalizer>

			<?php foreach($filesList['files'] as $file): ?>
				<div class="medium-3 column margin-bottom end file-list" data-equalizer-watch>
					<a href="<?php echo PLAYER_URL.$file ?>" class="">
						<img src="<?php echo getImageUrl('images/placeholder_list-3.png', 350, 200) ?>" width="100%" />
						<?php echo basename($file) ?>
					</a>
				</div>
			<?php endforeach; ?>

			<?php foreach($filesList['dirs'] as $file): ?>
				<div class="medium-3 column margin-bottom end file-list" data-equalizer-watch>
					<a href="<?php echo LIST_URL.$file ?>" class="">
						<?php $cover = getCover($file, 'placeholder_list.png'); ?>
						<?php if( $cover ): ?>
							<img src="<?php echo getImageUrl($cover, 350, 200) ?>" width="100%" />
							<br />
						<?php endif; ?>
						<?php echo formatName(basename($file)) ?>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
<?php include 'includes/footer.php' ?>
